package ru.stutmf.fintechprojectmentor.exception;

import com.fasterxml.jackson.annotation.JsonIgnore;

public enum ApplicationError {
    ENTITY_NOT_FOUND("Entity not found", 404),
    ENTITY_ALREADY_EXISTS("Entity is already present in database", 400),
    ENTITY_NULL_FIELDS("Entity has some required fields as null", 400);

    private final String message;
    private final int code;

    ApplicationError(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public ApplicationException exception(String args) {
        return new ApplicationException(this, args);
    }

    public static class ApplicationException extends RuntimeException {
        public final ApplicationExceptionCompanion companion;

        ApplicationException(ApplicationError error, String message) {
            super(error.message + ": " + message);
            this.companion = new ApplicationExceptionCompanion(error.code, error.message + ": " + message);
        }

        //@JsonIgnore is to avoid double code showing to user
        public record ApplicationExceptionCompanion(@JsonIgnore int code, String message){}
    }
    public String getMessage() { return message; }
    public int getCode() { return code; }
}
