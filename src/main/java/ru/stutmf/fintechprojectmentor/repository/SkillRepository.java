package ru.stutmf.fintechprojectmentor.repository;

import org.apache.ibatis.annotations.Mapper;
import ru.stutmf.fintechprojectmentor.model.Skill;

import java.util.List;
import java.util.Optional;

@Mapper
public interface SkillRepository {

    void save(Skill skill);

    void update(Skill skill);

    void delete(int id);

    Optional<Skill> findByName(String name);

    List<Skill> findAllSkills();
}
