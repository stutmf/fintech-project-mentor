package ru.stutmf.fintechprojectmentor.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TimePair {

    private LocalTime start;

    private LocalTime end;

    public TimePair (String start, String end) {
        this.start = LocalTime.parse(start);
        this.end = LocalTime.parse(end);
    }
}
