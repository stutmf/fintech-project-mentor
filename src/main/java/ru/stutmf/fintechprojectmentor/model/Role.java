package ru.stutmf.fintechprojectmentor.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Role {
    private Long id;
    private EnumRole name;

    public Role (EnumRole name) {
        this.name = name;
    }
}
