package ru.stutmf.fintechprojectmentor.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.stutmf.fintechprojectmentor.validation.MenteeConstraint;

@Data
@AllArgsConstructor
@NoArgsConstructor
@MenteeConstraint
public class Mentee {
    private int id;

    private String name;
    private String surname;
    private String middleName;
    private String project;

    private String username;

    public Mentee(String name, String username) {
        this.name = name;
        this.username = username;
    }
}
