package ru.stutmf.fintechprojectmentor.validation;

import ru.stutmf.fintechprojectmentor.model.Mentee;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static java.util.Objects.nonNull;

public class MenteeValidator implements ConstraintValidator<MenteeConstraint, Mentee> {

    @Override
    public boolean isValid(Mentee value, ConstraintValidatorContext context) {
        return value.getId() == 0 &&
                nonNull(value.getName());
    }
}
