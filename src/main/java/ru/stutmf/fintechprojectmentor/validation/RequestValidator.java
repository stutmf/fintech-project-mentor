package ru.stutmf.fintechprojectmentor.validation;

import org.springframework.util.StringUtils;
import ru.stutmf.fintechprojectmentor.model.Request;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static java.util.Objects.nonNull;

public class RequestValidator implements ConstraintValidator<RequestConstraint, Request>{

    @Override
    public boolean isValid(Request value, ConstraintValidatorContext constraintValidatorContext) {
        return  value.getId() == 0 &&
                value.getMentorId() != 0 &&
                value.getMenteeId() != 0 &&
                StringUtils.hasText(value.getDay().name()) &&
                nonNull(value.getStart()) &&
                nonNull(value.getEnd()) &&
                value.getStart().compareTo(value.getEnd()) < 0;
    }
}
