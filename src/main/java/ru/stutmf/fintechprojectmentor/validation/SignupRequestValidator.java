package ru.stutmf.fintechprojectmentor.validation;

import org.springframework.util.StringUtils;
import ru.stutmf.fintechprojectmentor.payload.request.SignupRequest;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SignupRequestValidator implements ConstraintValidator<SignupRequestConstraint, SignupRequest> {

    @Override
    public boolean isValid(SignupRequest value, ConstraintValidatorContext context) {
        return StringUtils.hasText(value.getUsername()) &&
                StringUtils.hasText(value.getEmail()) &&
                StringUtils.hasText(value.getPassword());
    }
}
