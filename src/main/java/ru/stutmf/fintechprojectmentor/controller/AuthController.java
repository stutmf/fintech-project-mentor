package ru.stutmf.fintechprojectmentor.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.stutmf.fintechprojectmentor.model.EnumRole;
import ru.stutmf.fintechprojectmentor.model.Role;
import ru.stutmf.fintechprojectmentor.model.User;
import ru.stutmf.fintechprojectmentor.payload.request.LoginRequest;
import ru.stutmf.fintechprojectmentor.payload.request.SignupRequest;
import ru.stutmf.fintechprojectmentor.payload.response.JwtResponse;
import ru.stutmf.fintechprojectmentor.payload.response.MessageResponse;
import ru.stutmf.fintechprojectmentor.repository.RoleRepository;
import ru.stutmf.fintechprojectmentor.repository.UserRepository;
import ru.stutmf.fintechprojectmentor.security.jwt.JwtUtils;
import ru.stutmf.fintechprojectmentor.security.services.UserDetailsImpl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static ru.stutmf.fintechprojectmentor.exception.ApplicationError.ENTITY_NULL_FIELDS;

@CrossOrigin(origins = "*", maxAge = 6000)
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/auth")
public class AuthController {

    private final AuthenticationManager authenticationManager;

    private final PasswordEncoder encoder;

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    private final JwtUtils jwtUtils;

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@RequestBody @Validated SignupRequest signUpRequest, BindingResult errors) {
        if (errors.hasErrors()) {
            throw ENTITY_NULL_FIELDS.exception("");
        }

        if (signUpRequest.getPassword().length() <= 3) {
            return ResponseEntity
                    .badRequest()
                    .body((new MessageResponse("Error: Password should have at least 4 characters")));
        }
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken"));
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use"));
        }

        // Create new user's account
        User user = new User(signUpRequest.getUsername(),
                signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()));

        Set<EnumRole> requestRoles = signUpRequest.getRoles();
        Set<Role> assignedRoles = new HashSet<>();

        /**
         * SignupRequest has EnumRole which checks if input has Role out of three [to date];
         * Otherwise BadRequest is thrown by Spring boot;
         * However validation with database is needed to be error-proof;
         */
        if (requestRoles == null) {
            Role userRole = repositoryFindByRole(EnumRole.ROLE_USER.name());
            assignedRoles.add(userRole);
        } else {
            requestRoles.forEach(role -> {
                assignedRoles.add(repositoryFindByRole(role.name()));
            });
        }

        user.setRoles(assignedRoles);
        userRepository.save(user);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@RequestBody @Validated LoginRequest loginRequest, BindingResult errors) {
        if (errors.hasErrors()) {
            throw ENTITY_NULL_FIELDS.exception("");
        }

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                roles));
    }

    private Role repositoryFindByRole(String roleName) {
        return roleRepository.findByName(roleName)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Given Role is not found in a database"));
    }
}
