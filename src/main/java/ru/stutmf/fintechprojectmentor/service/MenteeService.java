package ru.stutmf.fintechprojectmentor.service;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.stutmf.fintechprojectmentor.model.Mentee;
import ru.stutmf.fintechprojectmentor.repository.MenteeRepository;

import java.util.NoSuchElementException;

import static ru.stutmf.fintechprojectmentor.exception.ApplicationError.ENTITY_NOT_FOUND;

@AllArgsConstructor
@Service
public class MenteeService {
    private static final Logger log = LoggerFactory.getLogger(MenteeService.class);
    private final MenteeRepository repository;

    public void save(Mentee entity) {
        repository.save(entity);
        log.info("New Mentee with name = {} saved", entity.getName());
    }

    public void update(Mentee entity) {
        repository.update(entity);
        log.info("Mentee with name = {} updated", entity.getName());
    }

    @Transactional
    public void delete(int id) {
        checkIfEntityAbsent(id);

        repository.delete(id);
        log.info("Mentee with id = {} deleted", id);
    }

    public int getMyId(String name) {
        return repository.getMyId(name);
    }


    private void checkIfEntityAbsent(int id) {
        try {
            repository.find(id).orElseThrow();
        } catch (NoSuchElementException e) {
            log.info("Mentee with id = {} not found", id);
            throw ENTITY_NOT_FOUND.exception("");
        }
    }
}
