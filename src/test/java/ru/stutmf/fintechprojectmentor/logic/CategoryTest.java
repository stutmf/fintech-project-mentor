package ru.stutmf.fintechprojectmentor.logic;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import ru.stutmf.fintechprojectmentor.AbstractTest;
import ru.stutmf.fintechprojectmentor.model.Category;

import static java.lang.String.format;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
public class CategoryTest extends AbstractTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private MockMvc mockMvc;

    private final ObjectMapper mapper = new ObjectMapper();

    @WithMockUser(roles = "ADMIN")
    @Test
    void testAddNewCategorySuccess() throws Exception{
        var category = prepareValidCategory();
        var categoryJson = mapper.writeValueAsString(category);

        mockMvc.perform(post("/api/category")
                    .contentType("application/json")
                    .content(categoryJson))
                .andDo(print())
                .andExpect(status().isOk());

        var dbEntry =  execSelectByNameQuery(category.getName());
        Assertions.assertEquals(category, dbEntry);
    }

    @WithMockUser(roles = "USER")
    @Test
    void testAddNewCategoryFailUnauthorized() throws Exception{
        var category = prepareValidCategory();
        var categoryJson = mapper.writeValueAsString(category);

        mockMvc.perform(post("/api/category")
                        .contentType("application/json")
                        .content(categoryJson))
                .andDo(print())
                .andExpect(status().isForbidden());

        Assertions.assertThrows(EmptyResultDataAccessException.class, () ->
                execSelectByNameQuery(category.getName()));
    }

    @WithMockUser(roles = "ADMIN")
    @Test
    void testAddNewCategoryFail() throws Exception{
        var category = prepareInvalidCategory();
        var categoryJson = mapper.writeValueAsString(category);

        mockMvc.perform(post("/api/category")
                        .contentType("application/json")
                        .content(categoryJson))
                .andDo(print())
                .andExpect(status().isBadRequest());

        Assertions.assertThrows(EmptyResultDataAccessException.class, () ->
                execSelectByNameQuery(category.getName()));
    }

    @WithMockUser(roles = "ADMIN")
    @Test
    void testUpdateCategory() throws Exception{
        var category = prepareValidCategory();
        var categoryJson = mapper.writeValueAsString(category);

        mockMvc.perform(post("/api/category")
                        .contentType("application/json")
                        .content(categoryJson))
                .andDo(print())
                .andExpect(status().isOk());

        var newCategory = prepareUpdatedCategory();
        var newCategoryJson = mapper.writeValueAsString(newCategory);

        mockMvc.perform(put("/api/category")
                    .contentType("application/json")
                    .content(newCategoryJson))
                .andDo(print())
                .andExpect(status().isOk());
    }

    private Category execSelectByNameQuery (String name) {
        return jdbcTemplate.queryForObject(format("select * from public.categories where name='%s'", name),
                new BeanPropertyRowMapper<>(Category.class));
    }

    private Category prepareValidCategory() {
        return new Category("Wine", "Exquisite taste");
    }

    private Category prepareUpdatedCategory() {
        return new Category("Wine", "Fabulous taste");
    }

    private Category prepareInvalidCategory() {
        return new Category();
    }

    @AfterEach
    public void clearCategories(){
        jdbcTemplate.execute("delete from categories");
    }
}
