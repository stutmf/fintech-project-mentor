package ru.stutmf.fintechprojectmentor.logic;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import ru.stutmf.fintechprojectmentor.AbstractTest;
import ru.stutmf.fintechprojectmentor.model.EnumRole;
import ru.stutmf.fintechprojectmentor.payload.request.LoginRequest;
import ru.stutmf.fintechprojectmentor.payload.request.SignupRequest;

import java.util.HashSet;
import java.util.Set;

import static java.lang.String.format;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class AuthTest extends AbstractTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private MockMvc mockMvc;

    private final ObjectMapper mapper = new ObjectMapper();

    @Test
    public void testUserSignUpSuccess() throws Exception {
        String name = "TestUser";

        var user = prepareValidSignUpRequest(name);
        var userJson = mapper.writeValueAsString(user);

        mockMvc.perform(post("/api/auth/signup")
                    .contentType("application/json")
                    .content(userJson))
                .andExpect(status().isOk());

        var dbEntry = execSelectEmailByQuery(name);
        Assertions.assertEquals(user.getEmail(), dbEntry.getEmail());
    }

    @Test
    public void testUserSignUpFail() throws Exception {
        var user = prepareInvalidSignUpRequest();
        var userJson = mapper.writeValueAsString(user);

        mockMvc.perform(post("/api/auth/signup")
                        .contentType("application/json")
                        .content(userJson))
                .andExpect(status().isBadRequest());

        Assertions.assertThrows(EmptyResultDataAccessException.class, () ->
                execSelectEmailByQuery(user.getUsername()));
    }

    @Test
    public void testUserLoginSuccess() throws Exception {
        String name = "TestUser";

        var user = prepareValidSignUpRequest(name);
        var userJson = mapper.writeValueAsString(user);

        mockMvc.perform(post("/api/auth/signup")
                        .contentType("application/json")
                        .content(userJson))
                .andExpect(status().isOk());

        var userLogin = prepareValidLoginRequest(user.getUsername(), user.getPassword());
        var userLoginJson = mapper.writeValueAsString(userLogin);

        mockMvc.perform(post("/api/auth/signin")
                        .contentType("application/json")
                        .content(userLoginJson))
                .andExpect(status().isOk());

    }

    @Test
    public void testUserLoginFail() throws Exception {
        String name = "TestUser";

        var user = prepareValidSignUpRequest(name);
        var userJson = mapper.writeValueAsString(user);

        mockMvc.perform(post("/api/auth/signup")
                        .contentType("application/json")
                        .content(userJson))
                .andExpect(status().isOk());

        var userLogin = prepareInvalidLoginRequest(user.getUsername());
        var userLoginJson = mapper.writeValueAsString(userLogin);

        mockMvc.perform(post("/api/auth/signin")
                        .contentType("application/json")
                        .content(userLoginJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUserNotFoundLoginFail() throws Exception {
        String name = "TestUser";

        var user = prepareValidSignUpRequest(name);

        var userLogin = prepareValidLoginRequest(user.getUsername(), user.getPassword());
        var userLoginJson = mapper.writeValueAsString(userLogin);

        mockMvc.perform(post("/api/auth/signin")
                        .contentType("application/json")
                        .content(userLoginJson))
                .andExpect(status().isUnauthorized());
    }



    private SignupRequest execSelectEmailByQuery(String name) {
        return jdbcTemplate.queryForObject(format("select email from public.users where user_name='%s'", name),
                new BeanPropertyRowMapper<>(SignupRequest.class));
    }

    private SignupRequest prepareValidSignUpRequest(String name) {
        Set<EnumRole> roles = new HashSet<>();
        roles.add(EnumRole.ROLE_USER);

        return new SignupRequest(
                name,
                "trey@test.spring",
                "trey",
                roles
        );
    }

    private SignupRequest prepareInvalidSignUpRequest() {
        return new SignupRequest();
    }

    private LoginRequest prepareValidLoginRequest(String name, String password) {
        return new LoginRequest(name, password);
    }

    private LoginRequest prepareInvalidLoginRequest(String name) {
        return new LoginRequest(name, "");
    }

    @BeforeEach
    public void addRoles() {
        jdbcTemplate.execute("INSERT INTO roles " +
                "VALUES " +
                "(1, 'ROLE_ADMIN'), " +
                "(2, 'ROLE_MODERATOR'), " +
                "(3, 'ROLE_USER')");
    }

    @AfterEach
    public void clearTables(){
        jdbcTemplate.execute("DELETE FROM users");
        jdbcTemplate.execute("DELETE FROM roles");
    }
}
