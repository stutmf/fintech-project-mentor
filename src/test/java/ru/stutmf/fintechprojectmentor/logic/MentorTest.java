package ru.stutmf.fintechprojectmentor.logic;

import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import ru.stutmf.fintechprojectmentor.AbstractTest;
import ru.stutmf.fintechprojectmentor.model.*;
import ru.stutmf.fintechprojectmentor.service.MenteeService;
import ru.stutmf.fintechprojectmentor.service.MentorService;
import ru.stutmf.fintechprojectmentor.service.RequestService;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class MentorTest extends AbstractTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private MentorService mentorService;

    @Autowired
    private MenteeService menteeService;

    @Autowired
    private RequestService requestService;

    private final JsonMapper mapper = JsonMapper.builder().addModule(new JavaTimeModule()).build();


    @WithMockUser(roles = "MODERATOR")
    @Test
    void testAddNewMentorSuccess() throws Exception {
        var mentor = prepareValidMentor();
        var mentorJson = mapper.writeValueAsString(mentor);

        mentor.setUsername("user");

        mockMvc.perform(post("/api/mentor")
                        .contentType("application/json")
                        .content(mentorJson))
                .andDo(print())
                .andExpect(status().isOk());

        var dbEntry = execSelectByNameQuery(mentor.getName());
        Assertions.assertEquals(mentor.getName(), dbEntry.getName());
    }

    @WithMockUser(roles = "MODERATOR")
    @Test
    void testAddNewMentorFail() throws Exception {
        var mentor = prepareInvalidMentor();
        var mentorJson = mapper.writeValueAsString(mentor);

        mockMvc.perform(post("/api/mentor")
                        .contentType("application/json")
                        .content(mentorJson))
                .andDo(print())
                .andExpect(status().isBadRequest());

        Assertions.assertThrows(EmptyResultDataAccessException.class, () ->
                execSelectByNameQuery(mentor.getUsername()));
    }


    @WithMockUser(roles = "MODERATOR")
    @Test
    public void testDeleteMentorSuccess() throws Exception{
        var mentor = prepareValidMentor();
        var mentorJson = mapper.writeValueAsString(mentor);

        mockMvc.perform(post("/api/mentor")
                        .contentType("application/json")
                        .content(mentorJson))
                .andDo(print())
                .andExpect(status().isOk());

        int mentorId = execSelectIdByNameQuery(mentor.getName()).getId();
        String urlDelete = "/api/mentor/" + mentorId;

        mockMvc.perform(delete(urlDelete))
                .andDo(print())
                .andExpect(status().isOk());

        Assertions.assertThrows(EmptyResultDataAccessException.class, () ->
                execSelectByNameQuery(mentor.getUsername()));
    }

    @WithMockUser(roles = "MODERATOR")
    @Test
    public void testDeleteMentorFail() throws Exception{
        int mentorId = 0;
        String urlDelete = "/api/mentor/" + mentorId;

        mockMvc.perform(delete(urlDelete))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(content().string("{\"message\":\"Entity not found: \"}"));
    }

    @WithMockUser(roles = "MODERATOR")
    @Test
    public void testAcceptRequest() throws Exception {
        var mentor = prepareValidMentor();
        mentor.setUsername("user");
        mentorService.save(mentor);

        var mentee = prepareValidMentee();
        mentee.setUsername("user");
        menteeService.save(mentee);

        var request = prepareValidRequest(mentor.getId(), mentee.getId());
        requestService.save(request);

        mockMvc.perform(post("/api/mentor/acceptRequest")
                        .contentType("application/json")
                        .content(mapper.writeValueAsString(request.getId())))
                .andDo(print())
                .andExpect(status().isOk());

        Assertions.assertEquals("ACCEPTED",
                jdbcTemplate.queryForObject("SELECT * FROM requests",
                        new BeanPropertyRowMapper<>(Request.class)).getStatus().toString());

        Assertions.assertEquals(Boolean.TRUE, jdbcTemplate.queryForObject(
                format("SELECT is_taken FROM mentor_time JOIN persons p on p.person_id = mentor_time.mentor_id " +
                        "WHERE person_id=%s", mentor.getId()), Boolean.class));
    }

    @WithMockUser(roles = "MODERATOR")
    @Test
    public void testDeclineRequest() throws Exception {
        var mentor = prepareValidMentor();
        mentor.setUsername("user");
        mentorService.save(mentor);

        var mentee = prepareValidMentee();
        mentee.setUsername("user");
        menteeService.save(mentee);

        var request = prepareValidRequest(mentor.getId(), mentee.getId());
        requestService.save(request);

        mockMvc.perform(post("/api/mentor/declineRequest")
                        .contentType("application/json")
                        .content(mapper.writeValueAsString(request.getId())))
                .andDo(print())
                .andExpect(status().isOk());

        Assertions.assertEquals("DECLINED",
                jdbcTemplate.queryForObject("SELECT * FROM requests",
                        new BeanPropertyRowMapper<>(Request.class)).getStatus().toString());

        Assertions.assertEquals(Boolean.FALSE, jdbcTemplate.queryForObject(
                format("SELECT is_taken FROM mentor_time JOIN persons p on p.person_id = mentor_time.mentor_id " +
                        "WHERE person_id=%s", mentor.getId()), Boolean.class));
    }

    @WithMockUser(roles = "MODERATOR")
    @Test
    public void testGetPendingRequestsNone() throws Exception {
        mockMvc.perform(get("/api/mentor")
                        .contentType("application/json"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
    }

    @WithMockUser(roles = "MODERATOR")
    @Test
    public void testGetPendingRequestsOne() throws Exception {
        var mentor = prepareValidMentor();
        mentor.setUsername("user");
        mentorService.save(mentor);

        var mentee = prepareValidMentee();
        mentee.setUsername("user");
        menteeService.save(mentee);

        var request = prepareValidRequest(mentor.getId(), mentee.getId());
        requestService.save(request);

        String queryResult = prepareGetPendingRequestsResult(mentee, request); //stub

        mockMvc.perform(get("/api/mentor")
                        .contentType("application/json"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("[" + queryResult + "]"));
    }

    @WithMockUser(roles = "MODERATOR")
    @Test
    public void testGetPendingRequestsMany() throws Exception {
        var mentor = prepareValidMentor();
        mentor.setUsername("user");
        mentorService.save(mentor);

        var mentee = prepareValidMentee();
        mentee.setUsername("user");
        menteeService.save(mentee);

        var requestFirst = prepareValidRequest(mentor.getId(), mentee.getId());
        requestService.save(requestFirst);

        var requestSecond = prepareValidRequest(mentor.getId(), mentee.getId());
        requestSecond.setDay(DayOfWeek.FRIDAY);
        requestService.save(requestSecond);

        String firstQueryResult = prepareGetPendingRequestsResult(mentee, requestFirst); //stub
        String secondQueryResult = prepareGetPendingRequestsResult(mentee, requestSecond);

        mockMvc.perform(get("/api/mentor")
                        .contentType("application/json"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("[" + firstQueryResult + "," + secondQueryResult + "]"));
    }


    private MentorDto execSelectByNameQuery(String name) {
        return jdbcTemplate.queryForObject(format
                        ("select person_name as name, username from public.persons where person_name='%s'", name),
                new BeanPropertyRowMapper<>(MentorDto.class));
    }

    private MentorDto execSelectIdByNameQuery(String name) {
        return jdbcTemplate.queryForObject(format
                        ("select person_id as id from public.persons where person_name='%s'", name),
                new BeanPropertyRowMapper<>(MentorDto.class));
    }

    private Request prepareValidRequest(int mentorId, int menteeId) {
        var request = new Request();

        request.setMentorId(mentorId);
        request.setMenteeId(menteeId);
        request.setDay(DayOfWeek.MONDAY);
        request.setStart(LocalTime.parse("14:00"));
        request.setEnd(LocalTime.parse("15:00"));

        return request;
    }

    private MentorDto prepareValidMentor() {
        Map<DayOfWeek, List<TimePair>> map = new HashMap<>();
        List<TimePair> timePairs = new ArrayList<>();

        TimePair timePair = new TimePair("14:00", "15:00");
        timePairs.add(timePair);
        map.put(DayOfWeek.MONDAY, timePairs);

        List<Skill> skillList = new ArrayList<>();
        skillList.add(new Skill("GO", new Category("Programming Language")));

        return new MentorDto("Tagir", "+7919", "@valeev", map, skillList);
    }

    private Mentee prepareValidMentee() {
        return new Mentee("Lesha", "Vasilyev");
    }

    private MentorDto prepareInvalidMentor() {
        return new MentorDto();
    }

    private String prepareGetPendingRequestsResult(Mentee mentee, Request request) {
        DateTimeFormatter pattern = DateTimeFormatter.ofPattern("HH:mm:ss");

        return "{\"start_time\":\"" + request.getStart().format(pattern) +
                "\",\"end_time\":\"" + request.getEnd().format(pattern) +
                "\",\"person_name\":\"" + mentee.getName() +
                "\",\"request_id\":" + request.getId() +
                ",\"day\":\"" + request.getDay().name() +
                "\"}";
    }

    @AfterEach
    public void clearTables() {
        jdbcTemplate.execute("DELETE FROM mentor_skills; " +
                "DELETE FROM mentor_time; DELETE FROM persons; " +
                "DELETE FROM skills; DELETE FROM categories; " +
                "DELETE FROM users");
    }

    @BeforeEach
    public void fillTableCategories() {
        jdbcTemplate.execute("INSERT INTO categories VALUES ('Programming Language')");
        jdbcTemplate.execute("INSERT INTO skills VALUES ('GO', 'Programming Language')");
        jdbcTemplate.execute("INSERT INTO users(user_name, email, password) VALUES ('user', '@', 'qwerty')");

    }
}
